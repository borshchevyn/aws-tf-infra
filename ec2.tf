resource "aws_key_pair" "admin_pub_key" {
  key_name   = "admin_pub_key"
  public_key = "${file("${var.admin_pub_key}")}"
}

resource "aws_key_pair" "jump_host_pub_key" {
  key_name   = "jump_host_pub_key"
  public_key = "${file("${var.jump_host_pub_key}")}"
}

#create ec2 instance for jump host server 
resource "aws_instance" "jump-host" {

  count                       = 1
  ami                         = var.ami_rh
  instance_type               = var.instance_type
  vpc_security_group_ids      = [aws_security_group.allow_ssh.id,aws_security_group.allow_icmp.id]
  key_name                    = "${aws_key_pair.admin_pub_key.key_name}"
  
  user_data = templatefile("./files/jumphost_bootstrap.tpl",  { jump_host_priv_key = file("./keys/jump_host_key") })
  
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.infra-vpc-pub[0].id

  iam_instance_profile   = "${aws_iam_instance_profile.infra_ec2_s3_write_profile.name}"


  tags = {
    Name = join("", concat(["${var.project_name}-jmp0"], [count.index + 1]))
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_internet_gateway.infra-igw]

}

# create ec2 instance for web servers (RedHat ami)
resource "aws_instance" "infra-web-server" {

    count                  = var.web_instance_count
    ami                    = var.ami_rh
    instance_type          = var.instance_type
    key_name               = "${aws_key_pair.jump_host_pub_key.key_name}" 
    vpc_security_group_ids = [aws_security_group.allow_http_https.id,aws_security_group.allow_ssh.id,aws_security_group.allow_icmp.id]
    subnet_id              = aws_subnet.infra-vpc-priv[count.index].id

    iam_instance_profile   = "${aws_iam_instance_profile.infra_ec2_s3_read_profile.name}"
    user_data = file("./files/websrv_bootstrap.sh")

    tags = {
        Name = join("", concat(["${var.project_name} web0"], [count.index + 1]))
    }

    lifecycle {
        create_before_destroy = true
    }

  depends_on = [aws_nat_gateway.infra-nat]
}

