output "jumphost_public_ip" {
  value = aws_instance.jump-host.*.public_ip
}

output "web_servers_private_ip" {
    value = aws_instance.infra-web-server.*.private_ip
} 

output "s3_bucket" {
    value = aws_s3_bucket.infra-s3.bucket_domain_name
}