resource "aws_iam_policy" "infra_s3_read_policy" {
  name        = "infra-s3-read-policy"
  path        = "/"
  description = "Infra S3 read only access policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action : [
          "s3:Get*",
          "s3:List*"
        ],
        Effect   = "Allow"
        Resource = [
            "arn:aws:s3:::infra-test-stage-s3-bucket",
            "arn:aws:s3:::infra-test-stage-s3-bucket/*"
        ]              
      },
    ]
  })
  depends_on = [aws_s3_bucket.infra-s3]
}

resource "aws_iam_policy" "infra_s3_write_policy" {
  name        = "infra-s3-write-policy"
  path        = "/"
  description = "Infra S3 write access policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action : [
            "s3:*Object",
            "s3:List*"
        ],
        Effect   = "Allow"
        Resource = [
            "arn:aws:s3:::infra-test-stage-s3-bucket",
            "arn:aws:s3:::infra-test-stage-s3-bucket/*"
        ]
      },
    ]
  })
  depends_on = [aws_s3_bucket.infra-s3]
}

resource "aws_iam_role" "infra_ec2_s3_read_access" {
    name = "infra-ec2-s3-read-access"
    assume_role_policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [{
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }]    
    })
} 

resource "aws_iam_role" "infra_ec2_s3_write_access" {
    name = "infra-ec2-s3-write-access"
    assume_role_policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [{
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }]    
    })
} 

resource "aws_iam_policy_attachment" "infra_s3_read" {

    name = "infra-ec2-s3-read"
    roles = ["${aws_iam_role.infra_ec2_s3_read_access.name}"]
    policy_arn = "${aws_iam_policy.infra_s3_read_policy.arn}"

}

resource "aws_iam_policy_attachment" "infra_s3_write" {

    name = "infra-ec2-s3-write"
    roles = ["${aws_iam_role.infra_ec2_s3_write_access.name}"]
    policy_arn = "${aws_iam_policy.infra_s3_write_policy.arn}"

}

resource "aws_iam_instance_profile" "infra_ec2_s3_read_profile" {

    name = "infra-ec2-s3-read-profile"
    role = "${aws_iam_role.infra_ec2_s3_read_access.name}"

}

resource "aws_iam_instance_profile" "infra_ec2_s3_write_profile" {

    name = "infra-ec2-s3-write-profile"
    role = "${aws_iam_role.infra_ec2_s3_write_access.name}"

}


