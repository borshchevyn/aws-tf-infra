resource "aws_s3_bucket" "infra-s3" {
    bucket = "infra-test-stage-s3-bucket"
    
    lifecycle {
        prevent_destroy = false
    }

}