# allow incoming icmp traffic 
resource "aws_security_group" "allow_all_inside" {
  name        = "allow_all_inside"
  description = "allow all incoming traffic"
  vpc_id      = aws_vpc.infra-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.infra-vpc.cidr_block]
  }

  egress {
    description = "allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

}
# allow incoming icmp traffic 
resource "aws_security_group" "allow_icmp" {
  name        = "allow_icmp"
  description = "allow incoming icmp"
  vpc_id      = aws_vpc.infra-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.infra-vpc.cidr_block]
  }

  egress {
    description = "allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

}

# allow incoming ssh traffic 
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "allow incoming ssh"
  vpc_id      = aws_vpc.infra-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.admin_ip_net
  }

  egress {
    description = "allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

}

#allow incoming http/https traffic
resource "aws_security_group" "allow_http_https" {

  name        = "allow_http_https"
  description = "allow incoming http/https traffic"
  vpc_id      = aws_vpc.infra-vpc.id

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    description = "allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_https"
  }

  lifecycle {
    create_before_destroy = true
  }

}
