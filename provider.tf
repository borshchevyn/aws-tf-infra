# describe terraform provider and access
provider "aws" {
    region = "us-east-1"

    default_tags {
      tags = {
        Project     = "Infra test"
        Environment = "Stage"
      }
    }
}
