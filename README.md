Before 'terraform plan/apply':<br>
#################################<br> 
- Create a 'keys' folder in the current directory<br>
- Create ssh keys<br>
  ssh-keygen -t rsa -C "Jump host keys" -f ./keys/jump_host_key<br>
  ssh-keygen -t rsa -C "Admin keys" -f ./keys/admin_key<br>
 
- In file variables.tf add your public ip addresses in default values list of admin_ip_net variable<br>
  for example:<br>
  -------------------------------------------<br>
  variable "admin_ip_net" {<br>
      description = "administrators network"<br>
      type = list<br>
      default = ["10.10.0.0/16","1.1.1.1/32"]<br>
  }<br>

