variable "project_name" {
    default = "Infra test"
}

variable "web_instance_count" {
    description = "Web servers instance count"
    default = 2
}

variable "vpc_cidr_block" {
    description = "VPC cidr blocks"
    default = "10.10.0.0/16"
}

variable "vpc_cidr_block_priv" {
    description = "VPC cidr blocks"
    type = list
    default = ["10.10.21.0/24","10.10.22.0/24"]
}

variable "vpc_cidr_block_pub" {
    description = "VPC cidr blocks"
    type = list
    default = ["10.10.10.0/24"]
}

variable "admin_ip_net" {
    description = "administrators network"
    type = list
    default = ["10.10.0.0/16","46.158.83.161/32","46.159.114.95/32","85.174.235.155/32","85.172.14.211/32"]
}

variable "ami_rh" {
    description = "RedHat ami"
    default = "ami-0b0af3577fe5e3532"
}

variable "instance_type" {
    description = "EC2 instance type"
    default = "t2.micro"
}
 
variable "admin_pub_key" {
    default = "./keys/admin_key.pub" 
}

variable "jump_host_pub_key" {
    default = "./keys/jump_host_key.pub"
}
