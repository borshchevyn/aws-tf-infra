resource "aws_vpc" "infra-vpc" {

  cidr_block = var.vpc_cidr_block

  tags = {
      Name = "${var.project_name} VPC"
  }

}

resource "aws_subnet" "infra-vpc-priv" {

  vpc_id            = aws_vpc.infra-vpc.id
  count             = length(var.vpc_cidr_block_priv)
  cidr_block        = var.vpc_cidr_block_priv[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]
  
  tags = {
    Name = "${var.project_name} private subnet"
  }

}

resource "aws_subnet" "infra-vpc-pub" {

  vpc_id                  = aws_vpc.infra-vpc.id
  count                   = length(var.vpc_cidr_block_pub)
  cidr_block              = var.vpc_cidr_block_pub[count.index]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  
  tags = {
    Name = "${var.project_name} public subnet"
  }

}

resource "aws_route_table" "infra-vpc-public-rt" {
  vpc_id = aws_vpc.infra-vpc.id

  tags = {
    Name = "${var.project_name} public route table"
  }
}

resource "aws_route_table" "infra-vpc-private-rt" {
  vpc_id = aws_vpc.infra-vpc.id

  tags = {
    Name = "${var.project_name} private route table"
  }
}

resource "aws_route" "infra-vpc-public-igw" {

  route_table_id = aws_route_table.infra-vpc-public-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.infra-igw.id

}

resource "aws_route" "infra-vpc-private-nat" {

  route_table_id = aws_route_table.infra-vpc-private-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_nat_gateway.infra-nat.id

}

resource "aws_route_table_association" "vpc-infra-route-private" {

  count = length(var.vpc_cidr_block_priv)
  subnet_id      = element(aws_subnet.infra-vpc-priv.*.id,count.index)
  route_table_id = aws_route_table.infra-vpc-private-rt.id

}

resource "aws_route_table_association" "vpc-infra-route-public" {

  count = length(var.vpc_cidr_block_pub)
  subnet_id      = element(aws_subnet.infra-vpc-pub.*.id,count.index)
  route_table_id = aws_route_table.infra-vpc-public-rt.id

}

resource "aws_internet_gateway" "infra-igw" {

  vpc_id = aws_vpc.infra-vpc.id

  tags = {
      Name = "${var.project_name} IGW"
  }

}

# elastic ip for the nat gateway
resource "aws_eip" "infra-nat-eip" {

  vpc        = true
  depends_on = [aws_internet_gateway.infra-igw]

  tags = {
    Name = "${var.project_name} EIP"
  }

}

resource "aws_nat_gateway" "infra-nat" {

  allocation_id = aws_eip.infra-nat-eip.id
  subnet_id     = element(aws_subnet.infra-vpc-pub.*.id, 0)

    tags = {
        Name = "${var.project_name} NAT"
    }

    depends_on = [aws_internet_gateway.infra-igw]
}
